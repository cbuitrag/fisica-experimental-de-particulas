Estado final de nuestra senal(WZ):

Se esperan tres leptones cargados y un neutrino. Se esperan 0 Jets provenientes del vertice primario, sin embargo no se hace un corte para no perder datos.

Las variables utilizadas para llevar a cabo el analisis son la masa invariante de dos leptones (mismo sabor y de signo opuesto), porque esta sirve para reconstruir el Z. Tambien se utilizan la masa transversa calculada del candidato a W (utilizando el lepton restante) junto a la energia transversa perdida (neutrino), pues estos sirven para reconstruir el W. Para elegir los leptones buenos se utilizan las variables de isolation y las variables cinematicas de los leptones.

La senal de este proceso es muy clara, pero se espera que el background mas significativo sea el diboson(ZZ) debido a la presencia de multiples leptones en el estado final. Datasets utilizados: Todos menos Z y Zprime.
