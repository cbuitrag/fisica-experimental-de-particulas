#include <iostream>
#include <string>
#include <stdio.h>

void Data(){

	TFile *file = TFile::Open("Data_8TeV.root");

	TTree *tree = (TTree*) file->Get("mini");
	tree->Print();

	Bool_t e_trig;
	Bool_t mu_trig;
	Bool_t good_vtx;
	UInt_t lep_n;
	UInt_t jet_n;
	Float_t MET;
	Float_t MET_phi;

	Float_t lep_pt[10];  
	Float_t lep_eta[10];  
	Float_t lep_phi[10];  
	Float_t lep_E[10];  
	Int_t lep_type[10];  
	Float_t lep_ptcone30[10];
	Float_t lep_etcone20[10];

	Float_t jet_pt[10];
	Float_t jet_eta[10];
	Float_t jet_jvf[10];
	Float_t jet_MV1[10];

	tree->SetBranchAddress("trigE", &e_trig);
	tree->SetBranchAddress("trigM", &mu_trig);
	tree->SetBranchAddress("hasGoodVertex", &good_vtx);
	tree->SetBranchAddress("lep_n", &lep_n);
	tree->SetBranchAddress("jet_n", &jet_n);
	tree->SetBranchAddress("met_et", &MET);
	tree->SetBranchAddress("met_phi", &MET_phi);

	tree->SetBranchAddress("lep_pt", &lep_pt);
	tree->SetBranchAddress("lep_eta", &lep_eta);
	tree->SetBranchAddress("lep_phi", &lep_phi);
	tree->SetBranchAddress("lep_E", &lep_E);
	tree->SetBranchAddress("lep_type", &lep_type);
	tree->SetBranchAddress("lep_ptcone30", &lep_ptcone30);
	tree->SetBranchAddress("lep_etcone20", &lep_etcone20);

	tree->SetBranchAddress("jet_pt", &jet_pt);
	tree->SetBranchAddress("jet_eta", &jet_eta);
	tree->SetBranchAddress("jet_jvf", &jet_jvf);
	tree->SetBranchAddress("jet_MV1", &jet_MV1);

	TH1F *cutflow = new TH1F("Cutflow","Cutflow; Cut; Events",10,0,10);

	TH1F *hist_lep_pt = new TH1F("Lepton Pt","Lepton Pt; pT(GeV);Events",140,0,140);
	TH1F *hist_lep_trackiso = new TH1F("Lepton Track Isolation","Lepton Track Isolation; lep_ptcone30/lep_pt;Events",50,0,0.5);
	TH1F *hist_lep_caloiso = new TH1F("Lepton Calorimeter Isolation","Lepton Calorimeter Isolation; lep_etcone20/lep_pt;Events",50,0,0.5);
	TH1F *hist_lep_eta = new TH1F("Lepton eta","Lepton eta; eta;Events",52,-2.6,2.6);
	TH1F *hist_njet = new TH1F("Number of jets","number of jets; Jet multiplicity; Events",10,0,10);
	TH1F *hist_jet_pt = new TH1F("Leading Jet Pt","Leading jet Pt; pT(GeV);Events",200,0,200);
	TH1F *hist_jet_eta = new TH1F("Leading Jet eta","Leading jet eta; eta;Events",52,-2.6,2.6);
	TH1F *hist_jet_jvf = new TH1F("Leading Jet JVF","Leading jet JVF; JVF;Events",100,0,1);
	TH1F *hist_jet_mv1 = new TH1F("Leading Jet MV1","Leading jet MV1; MV1;Events",100,0,1);
	TH1F *hist_nbjet = new TH1F("Number of b-jets","number of b-jets; b-Jet multiplicity; Events",10,0,10);
	TH1F *hist_met = new TH1F("MET","MET; MET(GeV); Events",150,0,150);
	TH1F *hist_mtW = new TH1F("mtW","mtW; MtW(GeV); Events",150,0,150);

	int cut1 = 0;
	int cut2 = 0;
	int cut3 = 0;
	int cut4 = 0;
	int cut5 = 0;
	int cut6 = 0;
	int cut7 = 0;
	int cut8 = 0;

	Long64_t nentries = tree->GetEntries();
	for(int i = 0; i < nentries; i++)
	{
		tree->GetEntry(i);

		// cut1: good vertex
		if(!good_vtx) continue;
		cut1++;
		cutflow->Fill(1);

		// cut2: electron and muon trigger
		if(!e_trig && !mu_trig) continue;
		cut2++;
		cutflow->Fill(2);

		// preselection of good leptons
		int n_mu = 0;
		int n_el = 0;
		int n_lep = 0;
		int lep_index = 0;

		for(unsigned int i = 0; i < lep_n; i++){
			if(lep_pt[i] < 25000.) continue;
			if(lep_ptcone30[i]/lep_pt[i] > 0.15 ) continue; 
			if(lep_etcone20[i]/lep_pt[i] > 0.15 ) continue;
			// good muon
			if(lep_type[i] == 13 && TMath::Abs(lep_eta[i]) < 2.5){
				n_mu++;
				n_lep++;
				lep_index = i;
			}
			// good electron
			if(lep_type[i] == 11 && TMath::Abs(lep_eta[i]) < 2.47 && (TMath::Abs(lep_eta[i]) < 1.37 || TMath::Abs(lep_eta[i]) > 1.52)){
				n_el++;
				n_lep++;
				lep_index = i;
			}
		}

		// cut3: exactly one good lepton
		if(n_lep != 1) continue;
		cut3++;
		cutflow->Fill(3);

		// cut4: at least 4 jets
		if(jet_n < 4) continue;
		cut4++;
		cutflow->Fill(4);

		// selection of good jets
		int n_jets = 0;
		int n_bjets = 0;

		for(unsigned int j = 0; j < jet_n; j++){
			if(jet_pt[j] < 25000.) continue;
			if(TMath::Abs(jet_eta[j]) > 2.5) continue;
			if(jet_pt[j] < 50000. && TMath::Abs(jet_eta[j]) < 2.4 && jet_jvf[j] < 0.5) continue;
			n_jets++;
			if(jet_MV1[j] > 0.7892){
				n_bjets++;
			}
		}

		// cut5: at least 4 good jets
		if(n_jets < 4) continue;
		cut5++;
		cutflow->Fill(5);

		// cut6: at least two b-jets
		if(n_bjets < 2) continue;
		cut6++;
		cutflow->Fill(6);

		// cut7: MET > 30GeV
		if(MET < 30000.) continue;
		cut7++;
		cutflow->Fill(7);

		// TLorentzVector definition for Lepton and MET
		TLorentzVector Lepton = TLorentzVector();
		TLorentzVector MeT = TLorentzVector();
		Lepton.SetPtEtaPhiM(lep_pt[lep_index],lep_eta[lep_index],lep_phi[lep_index],lep_E[lep_index]);
		MeT.SetPtEtaPhiM(MET,0.,MET_phi,0.);

		// calculation of mTW
		float mTW = sqrt(2*Lepton.Pt()*MeT.Pt()*(1-TMath::Cos(Lepton.DeltaPhi(MeT))));

		// cut8: mTW > 30GeV
		if(mTW < 30000.) continue;
		cut8++;
		cutflow->Fill(8);

		// distribution plots after the cuts
		hist_lep_pt->Fill(lep_pt[lep_index]/1000.);
		hist_lep_trackiso->Fill(lep_ptcone30[lep_index]/lep_pt[lep_index]);
		hist_lep_caloiso->Fill(lep_etcone20[lep_index]/lep_pt[lep_index]);
		hist_lep_eta->Fill(lep_eta[lep_index]);
		
		hist_njet->Fill(jet_n);

		hist_jet_pt->Fill(jet_pt[0]/1000.);
		hist_jet_eta->Fill(jet_eta[0]/1000.);
		hist_jet_jvf->Fill(jet_jvf[0]);
		hist_jet_mv1->Fill(jet_MV1[0]);

		hist_nbjet->Fill(n_bjets);
		
		hist_met->Fill(MET/1000.);

		hist_mtW->Fill(mTW/1000.);

	}

	std::cout << "Done!" << std::endl;
	std::cout << "All events:" << nentries << std::endl;
	std::cout << "Cut1:" << cut1 << std::endl;
	std::cout << "Cut2:" << cut2 << std::endl;
	std::cout << "Cut3:" << cut3 << std::endl;
	std::cout << "Cut4:" << cut4 << std::endl;
	std::cout << "Cut5:" << cut5 << std::endl;
	std::cout << "Cut6:" << cut6 << std::endl;
	std::cout << "Cut7:" << cut7 << std::endl;
	std::cout << "Cut8:" << cut8 << std::endl;	

	TCanvas *canvas_cutflow_data = new TCanvas("Canvas_Cutflow_Data","",800,600);
	cutflow->Draw("");
	canvas_cutflow_data->Print("./homework_output_data/cutflow_data.pdf");

	TCanvas *canvas_hist_leppt_afterCut_data = new TCanvas("Canvas_hist_lepPt_afterCut_data","",800,600);
	hist_lep_pt->Draw("");
	canvas_hist_leppt_afterCut_data->Print("./homework_output_data/lepPt_afterCut_data.pdf");

	TCanvas *canvas_hist_leptrackiso_afterCut_data = new TCanvas("Canvas_hist_lepTrackIso_afterCut_data","",800,600);
	hist_lep_trackiso->Draw("");
	canvas_hist_leptrackiso_afterCut_data->Print("./homework_output_data/lepTrackIso_afterCut_data.pdf");

	TCanvas *canvas_hist_lepcaloiso_afterCut_data = new TCanvas("Canvas_hist_lepCaloIso_afterCut_data","",800,600);
	hist_lep_caloiso->Draw("");
	canvas_hist_lepcaloiso_afterCut_data->Print("./homework_output_data/lepCaloIso_afterCut_data.pdf");

	TCanvas *canvas_hist_lepeta_afterCut_data = new TCanvas("Canvas_hist_lepEta_afterCut_data","",800,600);
	hist_lep_eta->Draw("");
	canvas_hist_lepeta_afterCut_data->Print("./homework_output_data/lepEta_afterCut_data.pdf");

	TCanvas *canvas_hist_njet_afterCut_data = new TCanvas("Canvas_hist_njet_afterCut_data","",800,600);
	hist_njet->Draw("");
	canvas_hist_njet_afterCut_data->Print("./homework_output_data/njets_afterCut_data.pdf");

	TCanvas *canvas_hist_jetpt_afterCut_data = new TCanvas("Canvas_hist_jetPt_afterCut_data","",800,600);
	hist_jet_pt->Draw("");
	canvas_hist_jetpt_afterCut_data->Print("./homework_output_data/jetPt_afterCut_data.pdf");

	TCanvas *canvas_hist_jeteta_afterCut_data = new TCanvas("Canvas_hist_jetEta_afterCut_data","",800,600);
	hist_jet_eta->Draw("");
	canvas_hist_jeteta_afterCut_data->Print("./homework_output_data/jetEta_afterCut_data.pdf");

	TCanvas *canvas_hist_jetjvf_afterCut_data = new TCanvas("Canvas_hist_jetJVF_afterCut_data","",800,600);
	hist_jet_jvf->Draw("");
	canvas_hist_jetjvf_afterCut_data->Print("./homework_output_data/jetJVF_afterCut_data.pdf");

	TCanvas *canvas_hist_jetmv1_afterCut_data = new TCanvas("Canvas_hist_jetMV1_afterCut_data","",800,600);
	hist_jet_mv1->Draw("");
	canvas_hist_jetmv1_afterCut_data->Print("./homework_output_data/jetMV1_afterCut_data.pdf");

	TCanvas *canvas_hist_nbjet_afterCut_data = new TCanvas("Canvas_hist_nbjet_afterCut_data","",800,600);
	hist_nbjet->Draw("");
	canvas_hist_nbjet_afterCut_data->Print("./homework_output_data/nbjets_afterCut_data.pdf");

	TCanvas *canvas_hist_met_afterCut_data = new TCanvas("Canvas_hist_MET_afterCut_data","",800,600);
	hist_met->Draw("");
	canvas_hist_met_afterCut_data->Print("./homework_output_data/MET_afterCut_data.pdf");

	TCanvas *canvas_hist_mtW_afterCut_data = new TCanvas("Canvas_hist_mtW_afterCut_data","",800,600);
	hist_mtW->Draw("");
	canvas_hist_mtW_afterCut_data->Print("./homework_output_data/mtW_afterCut_data.pdf");

}